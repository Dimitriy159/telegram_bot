# -*- coding: utf-8 -*-
import os
import datetime

import telebot
import phonenumbers

from models import Users, Order
from messages import *
import cherrypy

API_TOKEN = '851137887:AAFi2Y2CxkDaBaC3m_0Zv4ksI48IDqZEOM8'
WEBHOOK_HOST = '188.166.27.101'
WEBHOOK_PORT = 88  # 443, 80, 88 или 8443 (порт должен быть открыт!)
WEBHOOK_LISTEN = '0.0.0.0'  # На некоторых серверах придется указывать такой же IP, что и выше

WEBHOOK_SSL_CERT = './webhook_cert.pem'  # Путь к сертификату
WEBHOOK_SSL_PRIV = './webhook_pkey.pem'  # Путь к приватному ключу

WEBHOOK_URL_BASE = "https://%s:%s" % (WEBHOOK_HOST, WEBHOOK_PORT)
WEBHOOK_URL_PATH = "/%s/" % (API_TOKEN)

bot = telebot.TeleBot(API_TOKEN)

den_chat_id = 271526226
san_chat_id = 401272944
iphone_choice = None
case_type_choice = None
last_order = None


class WebhookServer(object):
    @cherrypy.expose
    def index(self):
        if 'content-length' in cherrypy.request.headers and \
                        'content-type' in cherrypy.request.headers and \
                        cherrypy.request.headers['content-type'] == 'application/json':
            length = int(cherrypy.request.headers['content-length'])
            json_string = cherrypy.request.body.read(length).decode("utf-8")
            update = telebot.types.Update.de_json(json_string)
            # Эта функция обеспечивает проверку входящего сообщения
            bot.process_new_updates([update])
            return ''
        else:
            raise cherrypy.HTTPError(403)
        

def choice_product(message):
    markup = telebot.types.InlineKeyboardMarkup()
    product_type_choices = [
        'Silicone case',
        'Стекло 2.5D', 
        'Переходник',
        'Чехол для AirPods',
        'Прозрачный чехол',
        'Ремешок для Apple Watch',
    ]
    for idx, choice in enumerate(product_type_choices): 
        markup.add(
            telebot.types.InlineKeyboardButton(
                text=choice, callback_data='product_type_'+str(idx)
            )
        )
    bot.send_message(message.chat.id, text=second_step, reply_markup=markup)


def choice_iphone_model(message):
    # Iphone version choice
    markup = telebot.types.InlineKeyboardMarkup()
    iphone_choices = [
        '6/6s',
        '6/6s Plus', 
        '7/8',
        '7/8 Plus',
        'X/Xs',
        'Xr',
        'Xs Max'
    ]
    for choice in iphone_choices: 
        markup.add(
            telebot.types.InlineKeyboardButton(
                text=choice, callback_data='iphone_'+choice
            )
        )

    bot.send_message(message.chat.id, text=greeting1, reply_markup=markup)


@bot.message_handler(commands=['start'])
def start(message):
    user_id = message.from_user.id
    join_date = datetime.datetime.now()
    chat_id = message.chat.id

    if not Users.user_exists(user_id):
        Users.create(user_id=user_id, join_date=join_date, chat_id=chat_id)

    static_keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    btn_phone_model = telebot.types.KeyboardButton('Модель телефона')
    btn_aks = telebot.types.KeyboardButton('Аксессуары')
    static_keyboard.add(btn_phone_model, btn_aks)

    bot.send_message(message.chat.id, text=greeting, reply_markup=static_keyboard)    
    choice_iphone_model(message)


@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    if call.message:
        if call.data.startswith('iphone'):
            # Product type choice
            global iphone_choice
            iphone_choice = call.data[7:len(call.data)]
            choice_product(call.message)

        elif call.data.startswith('product_type_'):
            markup = telebot.types.InlineKeyboardMarkup()
            if str(0) in str(call.data.encode('utf-8')):
                photo = open('./images/last_cases.jpg', 'rb')
                global case_type_choice
                case_type_choice = 'iphone_case'
                bot.send_photo(call.message.chat.id, photo)
                bot.send_message(call.message.chat.id, text=after_image)
            
            elif str(1) in str(call.data.encode('utf-8')):
                photo = open('./images/IMG_5109.JPG', 'rb')
                markup.add(
                    telebot.types.InlineKeyboardButton(
                        text='Заказать', callback_data='order_glass'
                    )
                )
                bot.send_photo(call.message.chat.id, photo, reply_markup=markup)
            
            elif str(2) in str(call.data.encode('utf-8')):
                photo = open('./images/IMG_4837.JPG', 'rb')
                markup.add(
                    telebot.types.InlineKeyboardButton(
                        text='Заказать', callback_data='order_adapter'
                    )
                )
                bot.send_photo(call.message.chat.id, photo, reply_markup=markup)
                
            elif str(3) in str(call.data.encode('utf-8')):
                photo = open('./images/IMG_228.JPG', 'rb')
                case_type_choice = 'airpods_case'
                bot.send_photo(call.message.chat.id, photo)
                bot.send_message(call.message.chat.id, text=after_image)
            
            elif str(4) in str(call.data.encode('utf-8')):
                photo = open('./images/transparent_case.jpg', 'rb')
                markup.add(
                    telebot.types.InlineKeyboardButton(
                        text='Заказать', callback_data='order_transparent_case'
                    )
                )
                bot.send_photo(call.message.chat.id, photo, reply_markup=markup)
            
            elif str(5) in str(call.data.encode('utf-8')):
                photo = open('./images/remeshok.jpg', 'rb')
                case_type_choice = 'remeshok'
                bot.send_photo(call.message.chat.id, photo)
                bot.send_message(call.message.chat.id, text=after_image)
            
        elif call.data.startswith('order'):
            markup = telebot.types.InlineKeyboardMarkup()
            markup.add(
                telebot.types.InlineKeyboardButton(
                    text='Консультация', callback_data='consultation'
                )
            )
            global last_order
            if 'glass' in call.data:
                last_order = Order.create(user_id=call.message.chat.id, order_type='glass', iphone_choice=iphone_choice)
            elif 'adapter' in call.data:
                last_order = Order.create(user_id=call.message.chat.id, order_type='adapter', iphone_choice=iphone_choice)
            elif 'transparent' in call.data:
                last_order = Order.create(user_id=call.message.chat.id, order_type='transparent_case', iphone_choice=iphone_choice)

            bot.send_message(call.message.chat.id, text=fourth_step, reply_markup=markup)

        elif call.data.startswith('consultation'):
            bot.send_message(call.message.chat.id, text=after_consultation)

@bot.message_handler(func=lambda m: True)
def echo_all(message):
    if message.text == "Аксессуары":
        choice_product(message)
        return
    elif message.text == "Модель телефона":
        choice_iphone_model(message)
        return
    try:
        try:
            nums = [int(n) for n in message.text.split(',')]
        except:
            nums = [int(n) for n in message.text.split(' ')]
        
        if all(num in range(39) for num in nums) and case_type_choice:
            global last_order
            if case_type_choice is 'airpods_case':
                last_order = Order.create(
                    user_id=message.chat.id, order_type='airpods_case', case_number=message.text,
                    iphone_choice=iphone_choice
                )

            elif case_type_choice is 'iphone_case':
                last_order = Order.create(
                    user_id=message.chat.id, order_type='iphone_case', case_number=message.text,
                    iphone_choice=iphone_choice
                )

            elif case_type_choice is 'remeshok':
                last_order = Order.create(
                    user_id=message.chat.id, order_type='remeshok', case_number=message.text,
                    iphone_choice=iphone_choice
                )

            markup = telebot.types.InlineKeyboardMarkup()
            markup.add(
                telebot.types.InlineKeyboardButton(
                    text='Консультация', callback_data='consultation'
                )
            )
            bot.send_message(message.chat.id, text=fourth_step, reply_markup=markup)
        else:
            raise ValueError
    except ValueError:
        if '0' in message.text:
            phone_mes = message.text.strip().replace(" ", "").replace("(", "").replace(")", "")
            idx = phone_mes.find('0')
            if len(message.text) - idx >= 10:
                number = phonenumbers.parse(phone_mes[idx:idx+10], "UA")
                if phonenumbers.is_valid_number(number):
                    if iphone_choice and last_order:
                        bot.send_message(message.chat.id, text=after_order)
                        text_order = (
                            'Заказ: '  + '\n' + message.text + '\n' +
                            'Iphone: ' + last_order.iphone_choice  + '\n' +
                            'Тип заказа: ' + last_order.order_type + '\n' +
                            'Номер чехла: ' + str(last_order.case_number) + '\n'
                        )
                        bot.send_message(den_chat_id, text=text_order)
                        bot.send_message(san_chat_id, text=text_order)
                    else:
                        bot.send_message(message.chat.id, text='Спасибо, ожидайте звонка.')
                        bot.send_message(den_chat_id, text='Консультация: ' + message.text)
                        bot.send_message(san_chat_id, text='Консультация: ' + message.text)
                else:
                    bot.send_message(message.chat.id, text=error_data_mes)
            else:
                bot.send_message(message.chat.id, text=error_data_mes)
        else:
            bot.send_message(message.chat.id, text=error_data_mes)


bot.remove_webhook()

bot.set_webhook(url=WEBHOOK_URL_BASE + WEBHOOK_URL_PATH,
                certificate=open(WEBHOOK_SSL_CERT, 'r'))
        

cherrypy.config.update({
    'server.socket_host': WEBHOOK_LISTEN,
    'server.socket_port': WEBHOOK_PORT,
    'server.ssl_module': 'builtin',
    'server.ssl_certificate': WEBHOOK_SSL_CERT,
    'server.ssl_private_key': WEBHOOK_SSL_PRIV
})

cherrypy.quickstart(WebhookServer(), WEBHOOK_URL_PATH, {'/': {}})
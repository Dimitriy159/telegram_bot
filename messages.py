# -*- coding: utf-8 -*-

greeting = """
Здравствуйте!👋 
Вас приветствует магазин pchela.ua.
Цена: 
🔸Silicone cases - 149 грн.
🔸Прозрачный чехол - 79 грн.
🔸Чехол для AirPods - 129 грн.
🔸Ремешок для Apple Watch - 169 грн.
🔸Переходник (наушники + зарядка) - 99 грн. 
🔸Стекло 2.5D - 59 грн.


❗️У нас проходит акция: 
Silicone case + чехол для AirPods = 259 грн.
Silicone case + ремешок для Apple Watch = 309 грн.
Silicone case + стекло = 179 грн.
Silicone case+ переходник = 248грн.❗️
"""

greeting1 = """
Для того, чтобы заказать аксессуар - выберете вашу модель телефона ниже👇🏻
"""

second_step = """
Выберите какой аксессуар вам нужен👇🏻
"""

fourth_step = """
Для заказа,напишите ваши данные :
1)Ф.И.О 
2)Город 
3)Номер телефона 
4)Номер отделения новой почты 
"""

after_image = """Напишите номер цвета, для заказа😊"""

after_order = """Ваш заказ принят!
В течении 10 минут с вами свяжется менеджер для подтверждения заказа😊"""

after_consultation = """Оставьте свой номер телефона и наш менеджер свяжется с вами в течении 10 минут😊"""

error_data_mes = """Данные введены не верно, поробуйте снова!"""
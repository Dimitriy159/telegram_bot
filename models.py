from peewee import *

db = SqliteDatabase('users.db')


class BaseModel(Model):
    class Meta:
        database = db


class Users(BaseModel):
    user_id = IntegerField(unique=True)
    join_date = DateTimeField()
    chat_id = IntegerField(unique=True)

    @classmethod
    def get_user(cls, user_id):
        return cls.get(user_id == user_id)

    @classmethod
    def user_exists(cls, user_id):
        query = cls().select().where(cls.user_id == user_id)
        return query.exists()


class Order(BaseModel):
    user_id = IntegerField()
    order_type = CharField() # airpods_case, iphone_case, adapter, glass
    case_number = CharField(null=True)
    iphone_choice = CharField()

    @classmethod
    def get_orders(cls, order_id):
        return cls.get(id == order_id)

    @classmethod
    def user_exists(cls, order_id):
        query = cls().select().where(cls.id == order_id)
        return query.exists()
    
    @classmethod
    def get_last_order(cls, user_id):
        return cls().select().where(cls().user_id==user_id).order_by(cls().id).get()


# db.drop_tables([Order])
# db.create_tables([Order])